const express = require("express");
const knex = require("knex");

const dbConfig = require("./knexfile");
const app = express();
const port = 3000;


app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  const db = knex(dbConfig.development);
  db.raw('select 1+1 as result').then(function () {
    neededNext();
  }).catch(() => {
    throw new Error('No db connection');
  });
});

const routes = require('./routes/index');
routes(app);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };