const ee = require('events');

const statEmitter = new ee();
const stats = {
    totalUsers: 3,
    totalBets: 1,
    totalEvents: 1,
};

statEmitter.on('newUser', () => {
    stats.totalUsers++;
});
statEmitter.on('newBet', () => {
    stats.totalBets++;
});
statEmitter.on('newEvent', () => {
    stats.totalEvents++;
});

module.exports = { stats, statEmitter };
