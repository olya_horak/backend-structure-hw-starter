const betsRoutes = require('./betsRoutes');
const eventsRoutes = require('./eventsRoutes');
const healthRoutes = require('./healthRoutes');
const statsRoutes = require('./statsRoutes');
const transactionsRoutes = require('./transactionsRoutes');
const userRoutes = require('./usersRoutes');

module.exports = (app) => {
    app.use('/users', userRoutes);
    app.use('/health', healthRoutes);
    app.use('/transactions', transactionsRoutes);
    app.use('/events', eventsRoutes);
    app.use('/bets', betsRoutes);
    app.use('/stats', statsRoutes);
}